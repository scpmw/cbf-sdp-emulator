# This sets up a CI environemnt identical to the gitlab buildenv

apt-get -y update
apt-get -y install cmake libblas-dev liblapack-dev casacore-dev
python3 -m pip install -U pip
python3 -m pip install -r docker-requirements.txt
git clone https://github.com/OxfordSKA/OSKAR.git
mkdir OSKAR/oskar/ms/release
cd OSKAR/oskar/ms/release
cmake -DCASACORE_LIB_DIR=/usr/lib/x86_64-linux-gnu  ..
make -j4
make install
cd ../../../python
mv ../../3rdParty/setup_oskar_ms.py ./setup.py
python3 ./setup.py build
python3 ./setup.py install
cd ../../

