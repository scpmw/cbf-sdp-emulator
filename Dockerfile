# =============================================================================
# Visibility Receive image : build stage
# =============================================================================
FROM python:3.7-slim-buster AS buildenv

ARG BRANCH=master
ARG PYPI_REPOSITORY_URL=https://nexus.engageska-portugal.pt/repository/pypi
ARG OSKAR_REPO=https://github.com/OxfordSKA/OSKAR.git
ARG OSKAR_BRANCH=master

# Install system depdendencies
RUN apt-get -y update --fix-missing
RUN apt-get -y install cmake libblas-dev liblapack-dev casacore-dev
RUN apt-get -y install git
RUN apt-get -y install g++
RUN python3 -m pip install -U pip

# Build OSKAR MS library
WORKDIR /app
RUN git clone $OSKAR_REPO OSKAR \
 && cd OSKAR \
 && git checkout $OSKAR_BRANCH
RUN mkdir OSKAR/oskar/ms/release
WORKDIR /app/OSKAR/oskar/ms/release
RUN cmake -DCASACORE_LIB_DIR=/usr/lib/x86_64-linux-gnu  ..
RUN make -j4
RUN make install

# Checkout our project, don't build it yet
WORKDIR /app
RUN git clone https://gitlab.com/ska-telescope/cbf-sdp-emulator.git \
 && cd cbf-sdp-emulator \
 && git checkout $BRANCH

# Build OSKAR's python library using our copy of OSKAR's setup.py
WORKDIR /app/OSKAR/python
RUN cp /app/cbf-sdp-emulator/3rdParty/setup_oskar_ms.py ./setup.py
RUN python3 -m pip install .

# Install now our project
WORKDIR /app/cbf-sdp-emulator
RUN python3 -m pip install --extra-index-url=$PYPI_REPOSITORY_URL/simple .

# Second stage, install runtime-only libraries
FROM python:3.7-slim-buster
RUN apt-get update && apt-get install -y \
    libblas3 \
    liblapack3 \
    libcasa-ms3 \
 && rm -rf /var/lib/apt/lists/*
COPY --from=buildenv /usr/local /usr/local
