#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import find_packages, setup

with open('README.md') as readme_file:
    readme = readme_file.read()

setup(
    name='cbf-sdp-emulator',
    version='1.0.1',
    description="",
    long_description=readme + '\n\n',
    author="Stephen Ord",
    author_email='stephen.ord@csiro.au',
    url='https://github.com/steve-ord/cbf-sdp-emulator',
    packages=find_packages(),
    entry_points={
        'console_scripts': ['emu-send=cbf_sdp.packetiser:main', 'emu-recv=cbf_sdp.receiver:main']
    },
    include_package_data=True,
    license="BSD license",
    zip_safe=False,
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
    ],
    test_suite='tests',
    install_requires=[
        "astropy",
        "ska_logging",
        "spead2",
        "sh"
    ],
    extras_require={
        "plasma": ["sdp-dal-prototype"]
    },
    setup_requires=[
        # dependency for `python setup.py test`
        'pytest-runner',
    ]
    )
