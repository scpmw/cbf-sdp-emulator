#!/bin/bash
# Really simple send and recv.
# Just a few channels - so you can test it on your laptop. This is essentially
# the same as the receiver unit test. 
# Just launch the receiver - give it a second or two to start
# Then launch the sender
# Assuming you have a virtualenv installed in venv
# 
source ../../venv/bin/activate
#
# Start the plasma store
plasma_store -m 1000000000 -s /tmp/plasma &
# Need to start a PLASMA consumer that will connect to the store
# The DAL needs a registered consumer of the plasma store data

# Start the SPEAD2 receiver - this also fills the store
emu-recv -c ./plasma.conf &
# if you get a bind error you have a receiver still running somewhere
sleep 5
# Start sending the test data
# The visibility set is a simple point source simulation - but the layout is 4 LOW antennas
# There are 4 channels in the sumulation
emu-send -c ./plasma.conf ../../tests/data/gleam-vis.ms
# The received measurement set

