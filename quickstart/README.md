CBF-SDP Interface Emulator - Quick Start
========================================

Ok so you don't want to read all the documentation, or just want 
to get something running straight away. Open in the quickstart directory
and you will find some simple configuration files.

There are quickstart examples for the following situations:

1) A simple send and receive pair for a small number of channels on a single stream. 
The basic and simplest scheme - this will not expected to scale beyond a few hundred channels.

2) A simple send and receive pair for a larger number of channels using multiple streams
but a single output file. This employs a multi-threaded asynchronous receive and should scale.
Although the performance may be limited by disk performance - both sending and receiving.

Each experiment is in its own directory - example data sets are included in the tests/data
directory. You should not need to install anything other than this package to get them to
work.

Running the examples
------------------------------------

The example directories include a working configuration and run script ('run.sh'). Just executing the
run script will run a receiver in the background and a sender in the foreground. It will transfer the visibility 
data weights and flags and transfer the meta-data from the mock-TM interface.



