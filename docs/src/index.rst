.. skeleton documentation master file, created by
   sphinx-quickstart on Thu May 17 15:17:35 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


.. HOME SECTION ==================================================

.. Hidden toctree to manage the sidebar navigation.


CBF-SDP Emulator
================

These are all the packages, functions and scripts that form part of the project.

.. toctree::
  :maxdepth: 2

  installation
  receiver
  sender
  api
  quickstart
