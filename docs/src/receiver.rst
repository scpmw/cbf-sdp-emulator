Receiver
========

An ``emu-recv`` program should be available after installing the package.
This program receives packets. It is an extensible package that is built
to be agnostic to the actual mode of reception.


Configuration options
---------------------

Configuration options can be given through a configuration file or through the
command-line. See ``emu-recv -h`` for details. An example configuration file is
given in 'example.conf'.

The following configuration categories/names are supported for the SPEAD2
receivers - other receivers can be added that are not SPEAD2 compliant.
But currently as of this initial version we are only supplying the SPEAD2 UDP
receiver. The API for adding more receivers in this framework is supplied:

- ``reception``: these are configuration options applied
  when reading the input Measurement Set.

 - ``bind_hostname``: The IP address or hostname of the interface to which to bind for reception.
 - ``receiver_port_start``: The initial port number to which to bind.
 - ``num_ports``: [optional] The number of ports to which to bind. This can also be calculated from the 'channels_per_stream' option and the number of channels in the data-model.
 - ``consumer``: [spead2_mswriter] Which class to attach to the receiver. The default class is a measurement set writer. But others can be added.
 - ``datamodel``: This is important. We have no interface with the telescope model (TM) and therefore have to obtain all the observation metadata from somewhere. We have decided to use the Measurement Set as the basis for this. So you should supply a measurement set that contains the same metadata as that which is being sent. it does not have to be the same file - but the output measurement set parameters and the UVW will be taken from this file. We open and close this file quickly so there should be no issue with multiple open files. Providing the receiver is started before the sender.


- ``transmission``: these are options that generally apply
  to the transmission method and are mostly ignored by the receiver except:

 - ``channels_per_stream``: The number of channels for which data will be sent in a single stream. This is used in the case where multiple ports are required with multiple channels per port. You dont actually need this - but without it you have to set num_ports appropriately

Running
-------

The application is really simple to run. It is installed as an entrypoint when you
install the package and will run as simply as:
``emu_recv -c <configuration_file>``
Or you can specify the various options on the command line. A typical configuratio
file is supplied and looks like this::


    [transmission]

    method = spead2_transmitters
    target_host = 127.0.0.1
    target_port_start = 41000
    channels_per_stream = 1
    rate = 247000

    [reader]

    [payload]

    method = icd

    [reception]

    method = spead2_receivers
    receiver_port_start = 41000
    consumer = spead2_mswriter
    datamodel = tests/data/gleam-model.ms
    outputfilename = tests/data/recv-vis.ms

Note by default we use a spead2 transmitter and receiver and a ``icd`` payload.
We have designed this package to be extensible and if you want to add
different transmitters and receivers you should be able to

In practical terms it makes sense to start the receiver(s) before the transmitter
so they are waiting for data. But you do not have to - the protocols and consumers are
flexible enough to be started when the data-stream is already running.

Running Multiple Receivers
--------------------------

In many places we have made design decisions that are common to the SKA-SA systems. In the
case of multiple consumers we are simply using UDP multicast. THis means that multiple consumers
can access the same transmitted stream if they bind to the same multicast IP address and ports.
We have examples of this operations in the example configurations.

This multiple-receiver operation is required when displays and monitoring is required to sample the
data in transit - before it becomes a measurement set.


