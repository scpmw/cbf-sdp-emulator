.. doctest-skip-all
.. _package-guide:


API documentation
=================

This section describes requirements and guidelines.

Packetisers
-----------

We begin with the packetiser we have written as a default ``emulator``
this is a pretty simple package that uses the transmitter and payload classes
defined in the configuration to send data.

At the moment we have an assumption that the ICD payload is being used.
but minor changes to the packetise method would remove that requirement.
Very minimal work is needed to replicate this with another payload.

The actual transmission protocol is abstracted into the ``transmitters``
and this is currently defaulting to SPEAD2 and UDP. But as this is almost
completely abstracted should be easy to change.



.. automodule:: cbf_sdp.packetiser
    :members:


Transmitters
------------

The transmitters are envisaged to be at least as diverse as UDP, IBV
and perhaps ROCE we have only implemented the UDP transmitter. But extensions
should be trivial

.. automodule:: cbf_sdp.transmitters.spead2_transmitters
    :members:

Payloads
--------

.. automodule:: cbf_sdp.payloads.icd
    :members:



Receivers
---------

.. automodule:: cbf_sdp.receivers.spead2_receivers
    :members:


Others
------

.. autoclass:: cbf_sdp.utils.FakeTM
   :members:
