.. module:: ../cbf_sdp/transmitters/spead2_transmitters.py

Sender
======

An ``emu-send`` program should be available after installing the package.
This program takes a Measurement Set and transmits it over the network
using the preferred transmission method.


Configuration options
---------------------

Configuration options can be given through a configuration file or through the
command-line. See ``emu-send -h`` for details.

The following configuration categories/names are supported:

- ``reader``: these are configuration options applied
  when reading the input Measurement Set.

 - ``start_chan``: the first channel for which data is read.
   Channels before this one are skipped.
   If ``start_chan`` is bigger
   than the actual number of channels in the input MS
   an error is raised.
 - ``num_chan``: number of channels for which data is read.
   If ``num_chan`` + ``start_chan`` are bigger
   than the actual number of channels in the input MS
   then ``num_chan`` is adjusted.
 - ``num_repeats``: number of times a single set of visibilities
   should be sent after being read, defaults to ``1``.
   Bigger values will send the same data over and over,
   which is less realistic but imposes less stress on the file-system.

- ``transmission``: these are options that apply
  to the transmission method.

 - ``method``: the transmission method to use, defaults to ``spead2``.
 - ``target_host``: the host where data will be sent to.
 - ``target_port_start``: the first port where data will be sent to.
 - ``channels_per_stream``: number of channels for which data will be sent
   in a single stream.
 - ``max_packet_size``: the maximum size of packets to build, used by
   ``spead2``.
 - ``rate``: the maximum send data rate, in bytes/s.
   Used by ``spead2``, defaults to 1 GB/s.
