try:
    import sdp_dal.plasma as rpc
except ImportError:
    _sdp_dal_found = False
else:
    _sdp_dal_found = True

import pyarrow
import numpy
import pickle

import time
import sys

# define a sort of entry point
# this has a parameter - the input
# and a tag - the tag is used because in this stream logic
# there needs to be someway of tracking what is done.
# this allows the call method to be launched using call.simple


SIMPLE_PROCS = [
    rpc.make_call_schema('unpickle', [
        rpc.make_tensor_input_par('input', pyarrow.int8(), ['x']),
        rpc.make_tensor_output_par('tag', pyarrow.int8(), []),
    ]), rpc.make_call_schema('writems', [
        rpc.make_tensor_input_par('config', pyarrow.int8(), ['x']),
        rpc.make_tensor_input_par('model', pyarrow.int8(), ['x']),
        rpc.make_tensor_input_par('payload', pyarrow.int8(), ['x']),
        rpc.make_tensor_output_par('tag', pyarrow.int8(), []),
    ])
]

# This you can call what you want - but it must inherit from rpc.Caller 
                        
class SimpleCaller(rpc.Caller):

    def __init__(self, *args, **kwargs):
        super(SimpleCaller, self).__init__(SIMPLE_PROCS, *args, **kwargs)

# This is the processor - generally used by the receive end ...
# You can also call this what you want but it must inherit from rpc.Processor
 
class SimpleProcessor(rpc.Processor):
    # This must call the __init__ of the base class

    def __init__(self, *args, **kwargs):
        super(SimpleProcessor, self).__init__(SIMPLE_PROCS, *args, **kwargs)
        self.bytes_received = 0
    
    # we must have a process_call
    def _process_call(self, proc_name, batch):
        print("In _process_call")
        # this is the process name from the make_call_schema at the top
        if proc_name == 'unpickle':
            rtn = self._unpickle(batch)
            if rtn == False:
                raise ValueError("Unpickle failed")
            else:
                raise KeyboardInterrupt
        elif proc_name == 'writems':
            rtn = self._writems(batch)
            if rtn == False:
                raise ValueError("Unpickle failed")
            else:
                raise KeyboardInterrupt
        else:
            raise ValueError("Call to unknown procedure {}!".format(proc_name))



    def _unpickle(self, batch):
        print("In unpickle -- just used for initial testing")
        # get input
        inp = self.tensor_parameter(batch, 'input', pyarrow.int8())
        # now to turn this into a byte stream and unpickle it
        # test as before in the really simple example
        # Log number of bytes received
        self.bytes_received += memoryview(inp).nbytes
        view = memoryview(inp)
        bytes = view.tobytes()
        payload = pickle.loads(bytes)

        if payload.vis[0] != 1.0:
            return False
        else:
            return True

    def _writems(self,batch):
        print("In writems")

        config_pickle = self.tensor_parameter(batch, 'config', pyarrow.int8())
        model_pickle = self.tensor_parameter(batch, 'model', pyarrow.int8())
        payload_pickle = self.tensor_parameter(batch, 'payload', pyarrow.int8())

        self.bytes_received += memoryview(config_pickle).nbytes
        view = memoryview(config_pickle)
        bytes = view.tobytes()
        self.config = pickle.loads(bytes)

        self.bytes_received += memoryview(model_pickle).nbytes
        view = memoryview(model_pickle)
        bytes = view.tobytes()
        self.model = pickle.loads(bytes)

        self.bytes_received += memoryview(payload_pickle).nbytes
        view = memoryview(payload_pickle)
        bytes = view.tobytes()
        payload = pickle.loads(bytes)

        if payload.vis[0] != 1.0:
            return False
        else:
            return True
