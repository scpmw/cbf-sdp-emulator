# -*- coding: utf-8 -*-
"""Reads data off a MS and creates SPEAD2 packets"""

import argparse
import asyncio
import configparser
import logging

import ska.logging

from cbf_sdp import receivers, utils


logger = logging.getLogger(__name__)
DEFAULT_CONFIG_FILE = 'receiver.conf'


def _config_parser(f):
    config_parser = configparser.ConfigParser()
    config_parser.read(f)
    if 'reception' not in config_parser:
        config_parser['reception'] = {}
    if 'transmission' not in config_parser:
        config_parser['transmission'] = {}
    if 'reader' not in config_parser:
        config_parser['reader'] = {}
    if 'payload' not in config_parser:
        config_parser['payload'] = {}
    return config_parser


def _augment_config(config, options):
    for opt in options:
        name, value = opt.split('=')
        category, name = name.split('.')
        if category not in config:
            config[category] = {}
        config[category][name] = value

def main():

    parser = argparse.ArgumentParser(
        description="Receives from a UDP socket"
    )
    parser.add_argument(
        "-c",
        '--config',
        help="The configuration file to load, defaults to %s" % DEFAULT_CONFIG_FILE,
        default=DEFAULT_CONFIG_FILE,
        type=_config_parser,
    )
    parser.add_argument(
        "-o",
        "--option",
        help="Additional configuration options in the form of category.name=value",
        action='append'
    )
    parser.add_argument(
        "-v",
        "--verbose",
        help="If set, more verbose output will be produced",
        action="store_true"
    )

    args = parser.parse_args()
    logging_level = logging.DEBUG if args.verbose else logging.INFO
    ska.logging.configure_logging(level=logging_level)
    config = args.config
    if (args.option):
        _augment_config(config, args.option)

    tm = utils.FakeTM(config['reception'].get('datamodel',""))
    loop = asyncio.get_event_loop()
    loop.run_until_complete(receivers.create(config, tm, loop).run())


if __name__ == '__main__':
    main()
