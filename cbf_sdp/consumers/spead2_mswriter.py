# -*- coding: utf-8 -*-


""" Takes a SPEAD2 HEAP and writes it to a MEASUREMENT SET. This is pretty much the 
        same functionality as presented in the  OSKAR python binding example available at:
        https://github.com/OxfordSKA/OSKAR/blob/master/python/examples/spead/receiver/spead_recv.py
"""
import asyncio
import concurrent.futures

from cbf_sdp import utils


class consumer(utils.MSWriter):
    """
    A heap consumer that writes incoming data into an MS.

    Because data consumption happens inside the event loop we need to defer the
    data writing to a different thread. We do this by creating a single-threaded
    executor that we then use to schedule the I/O-heavy MS writing tasks onto.
    """
    
    def __init__(self, config, tm):
        super().__init__(config['reception'].get('outputfilename', 'recv-vis.ms'),
              tm.num_baselines, tm.num_stations, tm.num_channels, tm.num_pols,
              tm.freq_start_hz, tm.freq_inc_hz, tm.is_autocorrelated,
              tm.phase_centre_radec_rad)
        self.tm = tm
        self.executor = concurrent.futures.ThreadPoolExecutor(max_workers=1)

    async def consume(self, payload):
        """
        Entry point invoked by the receiver each time a heap arrives

        :param heap: An incoming Heap
        """

        # Find out the time index and TM data for this timestamp
        payload_time = payload.mjd_time
        try:
            time_idx, data = self.tm.get_matching_data(payload_time)
            row = time_idx * self.tm.num_baselines
        except ValueError:
            raise ValueError("No time to match {payload_time}")

        assert len(data.uu) == self.tm.num_baselines, \
               (f"Miss-match between baselines in heap {self.tm.num_baselines} "
                "and uvw vector {len(data.uu)}")

        # Write coord and vis data (in an executor to avoid clogging the event loop)
        vis = payload.visibilities
        first_chan = payload.channel_id
        chan_count = payload.channel_count
        loop = asyncio.get_event_loop()
        await loop.run_in_executor(self.executor, self.write_data,
            row, payload_time, data.interval, data.exposure,
            first_chan, chan_count, data.uu, data.vv, data.ww, vis)