# -*- coding: utf-8 -*-


""" Takes a SPEAD2 HEAP and writes it to an apache plasma store. This uses the sdp-dal-prototype
    API and will fail if that cannot be loaded
"""


import pickle
import warnings

import numpy

try:
    import pyarrow
    import pyarrow.plasma as plasma
except ImportError:
    warnings.warn("pyarrow not installed", ImportWarning)
    _has_pyarrow = False
else:
    _has_pyarrow = True

try:
    import sdp_dal.plasma as rpc
except ImportError:
    warnings.warn("sdp_dal not found", ImportWarning)
    _has_sdp_dal = False
else:
    _has_sdp_dal = True

from cbf_sdp import plasma_processor as proc
import asyncio
import concurrent.futures
import logging


logger = logging.getLogger(__name__)


class consumer(object):
    """
    A heap consumer that writes incoming data into a Plasma Store.

    Because data consumption happens inside the event loop we need to defer the
    data writing to a different thread. We do this by creating a single-threaded
    executor that we then use to schedule the access to the Plasma Store.
    """
    
    def __init__(self, config,tm):
        self.tm = tm
        self.store = rpc.Store((config['reception'].get('plasma_path', '/tmp/plasma')))
        self.call = proc.SimpleCaller(self.store, broadcast=True, verbose=True)
        self.executor = concurrent.futures.ThreadPoolExecutor(max_workers=1)
        self.parallelism = 50
        self.config = config
        self.config_ref = 0
        self.model_ref = 0
        self.tag_refs = []



    async def consume(self, payload):
        """
        Entry point invoked by the receiver each time a heap arrives

        :param payload: the unpacked payload
        """
        if self.config_ref == 0:
            self._write_config_to_plasma()
        if self.model_ref == 0:
            self._write_model_to_plasma()

        loop = asyncio.get_event_loop()

        await loop.run_in_executor(self.executor, self._write_data_to_plasma,
             payload)

    def _write_config_to_plasma(self):
        config_pickle = pickle.dumps(self.config)
        config_array = numpy.frombuffer(config_pickle, 'int8', -1, 0)
        # this has to remain in scope to stay in the store
        self.config_ref = self.store.put_new_tensor(config_array, pyarrow.int8())

    def _write_model_to_plasma(self):
        model_pickle = pickle.dumps(self.tm)
        model_array = numpy.frombuffer(model_pickle, 'int8', -1, 0)
        # this has to remain in scope to stay in the store
        self.model_ref = self.store.put_new_tensor(model_array, pyarrow.int8())


    def _write_data_to_plasma(self, payload):

        # we need to send the block of visibilities. But we need to include some meta data
        # we are essentially serialising this payload

        # going to test with an array
        data = pickle.dumps(payload)
        data_array = numpy.frombuffer(data, 'int8', -1, 0)

        # pop old data blocks off the store to stop it filling up

        if len(self.tag_refs) >= self.parallelism:  # this is how many blocks will be in the store
            refs = self.tag_refs.pop(0)  # get the top of the list
            # each entry is a OiD from the 'put'
            # {'tag': <sdp_dal.plasma.store.TensorRef object>}
            for ref in refs:
                ref['tag'].get(0.1)
                # this gets the object from storage and *deletes* it
                # this argument is just a timeout

        payload = self.store.put_new_tensor(data_array,pyarrow.int8())
        self.tag_refs.append(payload)
        # we now need to send the model
        # pylint: disable=no-member
        self.tag_refs.append(self.call.writems(self.config_ref,self.model_ref, payload))
        # pylint: disable=no-member
        # self.tag_refs.append(self.call.unpickle(payload))


