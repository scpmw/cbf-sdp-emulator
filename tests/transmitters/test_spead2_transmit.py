#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for the cbf_sdp module."""
import asyncio

from cbf_sdp import transmitters
import numpy as np


def _test_with_chans_per_stream(chan_per_stream):

    async def runner(loop):
        num_baselines = 10
        num_channels = 16
        config = {
            'transmission': {
                'method': 'spead2_transmitters',
                'target_host': '127.0.0.1',
                'target_port_start': '41000',
                'channels_per_stream': chan_per_stream,
            },
            'payload': {
                'method': 'icd'
            }
        }

        vis = np.zeros(shape=(num_channels, num_baselines, 4), dtype='<c8')

        transmitter = transmitters.create(config['transmission'], num_baselines, num_channels, loop)
        async with transmitter:
            await transmitter.send(0, 0, vis)

    loop = asyncio.new_event_loop()
    loop.run_until_complete(runner(loop))

def test_single_stream():
    _test_with_chans_per_stream(4)

def test_multiple_streams():
    """Test multiple stream generation"""
    _test_with_chans_per_stream(2)