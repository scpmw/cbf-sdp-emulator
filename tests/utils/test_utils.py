#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for the cbf_sdp module."""
import pytest

from cbf_sdp import utils

def test_package():
    tm = utils.FakeTM("tests/utils/sim-vis.ms")

    # I know that for this test measurement set that the time dump below has the 3rd baseline equal to
    # 4941813965.129038 -558.0393638815731 -692.2877732999623 -0.7558897212147713

    time_idx, data = tm.get_matching_data(4941813965.129038)

    assert time_idx == 2, "Time index was supposed to be 0"
    assert data.time == 4941813965.129038, "Time was supposed to match but does not"
    assert data.uu[2] == -558.0393638815731, "U distance in m was supposed to match but does not"
    assert data.vv[2] == -692.2877732999623, "V distance in m was supposed to match but does not"
    assert data.ww[2] == -0.7558897212147713, "W distance in m was supposed to match but does not"

def fail_file():

    utils.FakeTM("IamNotHere")

def test_fail_file():
    with pytest.raises(IOError):
        fail_file()
