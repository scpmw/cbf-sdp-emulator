#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for the cbf_sdp module."""
import asyncio
import tempfile
import unittest

import numpy
import oskar

from cbf_sdp import packetiser, utils
from cbf_sdp.receivers import spead2_receivers


INPUT_FILE = 'tests/receivers/sim-vis.ms'

class TestSpead2Receiver(unittest.TestCase):

    def _get_config(self, output_file, channels_per_stream):
        return {
            'reception': {
                'method': 'spead2_receivers',
                'receiver_port_start': 41001,
                'consumer': 'spead2_mswriter',
                'datamodel':INPUT_FILE,
                'outputfilename': output_file,
                # each stream will receive 133 heaps in total, so let's make
                # sure we don't overflow the ringbuffer while writing
                'ring_heaps': 133
            },
            'transmission': {
                'method': 'spead2_transmitters',
                'target_host': '127.0.0.1',
                'target_port_start': 41001,
                'channels_per_stream': channels_per_stream,
                'rate': 147000
            },
            'reader':  {}
        }

    async def _test_send_recv_stream(self, loop, num_streams):
        """Example: Assert the spead2 receiver code."""
        # test file is 4 antennas, 4 channels
        # 0.9 second dumps
        # rate should be ~1422 B/s for data
        # plus 48 B for the header
        # == 1470 B/s

        output_file = tempfile.mktemp(".ms", "output")
        tm = utils.FakeTM(INPUT_FILE)
        config = self._get_config(output_file, tm.num_channels // num_streams)
        receiver = spead2_receivers.receiver(config, tm, loop)
        receiving = receiver.run()
        sending = packetiser.packetise(config, INPUT_FILE)
        await asyncio.gather(receiving, sending)

        in_ms = oskar.MeasurementSet.open(INPUT_FILE, readonly=True)
        out_ms = oskar.MeasurementSet.open(output_file, readonly=True)
        self.assertEqual(in_ms.num_channels, out_ms.num_channels)
        self.assertEqual(in_ms.num_pols, out_ms.num_pols)
        self.assertEqual(in_ms.num_rows, out_ms.num_rows)
        self.assertEqual(in_ms.num_stations, out_ms.num_stations)

        first_vis_in, first_vis_out = (ms.read_vis(0, 0, ms.num_channels, 1)
                                       for ms in (in_ms, out_ms))
        last_vis_in, last_vis_out = (ms.read_vis(ms.num_rows - 1, 0, ms.num_channels, 1)
                                       for ms in (in_ms, out_ms))
        numpy.testing.assert_array_equal(first_vis_in, first_vis_out)
        numpy.testing.assert_array_equal(last_vis_in, last_vis_out)

    def test_single_stream(self):
        loop = asyncio.new_event_loop()
        loop.run_until_complete(self._test_send_recv_stream(loop, 1))

    def test_two_streams(self):
        loop = asyncio.new_event_loop()
        loop.run_until_complete(self._test_send_recv_stream(loop, 2))

    def test_four_streams(self):
        loop = asyncio.new_event_loop()
        loop.run_until_complete(self._test_send_recv_stream(loop, 4))