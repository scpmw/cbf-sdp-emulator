#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Tests for the cbf_sdp module."""
from cbf_sdp import icd


def test_time():
    mjd = icd.icd_to_mjd(1587095634, 0)
    assert mjd == 5093812434.0, "MJD sec should be 5093812434.0 for 2020-04-17T03:53:54+00:00"