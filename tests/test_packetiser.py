# -*- coding: utf-8 -*-

import asyncio

from cbf_sdp import packetiser


def test_package():
    config = {}
    config['reader'] = {'start_chan' : 0}
    config['reader'] = {'num_chan' : 4}
    config['transmission'] = {'method' : 'spead2_transmitters'}
    config['transmission'] = {'target_host' : '127.0.0.1'}
    config['transmission'] = {'target_port_start' : 41000}
    config['transmission'] = {'channels_per_stream' : 4}
    config['payload'] = { 'method' : 'icd' }
    asyncio.run(packetiser.packetise(config, "tests/data/sim-vis.ms"))