#!/usr/bin/env python
from cbf_sdp import consumers, utils
import tempfile


def test_mswriter_setup ():
    output_ms = tempfile.mktemp(".ms", "output")
    config = {
        'reception': {"outputfilename": output_ms},
    }
    INPUT_FILE = 'tests/data/sim-vis.ms'
    myconsumer = consumers.create(config, utils.FakeTM(INPUT_FILE))
    assert myconsumer