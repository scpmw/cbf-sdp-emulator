#!/usr/bin/env python


import asyncio
import subprocess as sp
import unittest
import numpy

from cbf_sdp import consumers, utils, plasma_processor,icd

class TestPlasmaWriter(unittest.TestCase):


    def setUp(self):
        self._store = sp.Popen(["plasma_store", "-m", "100000000", "-s", "/tmp/plasma"])
    def tearDown(self):
        self._store.terminate()

    def _test_plasma_writer_config (self):

        self.config = {
        'reception': {"consumer": "plasma_writer", "test_entry": "5", "plasma_path" : "/tmp/plasma"}
        }

    async def _create_plasma_writer(self):
        # this needs to be running for the consumer to work
        proc = plasma_processor.SimpleProcessor((self.config['reception'].get('plasma_path', '/tmp/plasma')))
        INPUT_FILE = 'tests/utils/sim-vis.ms'
        try:
            c = consumers.create(self.config, utils.FakeTM(INPUT_FILE))
        except RuntimeError:
            raise RuntimeError
        return True


    async def _run_plasma_writer(self):
        # wait so the proc is up
        await asyncio.sleep(2.0)
        INPUT_FILE = 'tests/utils/sim-vis.ms'
        c = consumers.create(self.config, utils.FakeTM(INPUT_FILE))
        DATA_SIZE = 100000
        while True:
            try:
                await asyncio.sleep(1.0)
                payload = icd.Payload()
                payload.vis = numpy.arange(DATA_SIZE, dtype='float64')
                payload.vis[0] = 1.0
                await c.consume(payload)
                await asyncio.sleep(10.0)
                break
            except RuntimeError:
                raise RuntimeError
            except ValueError:
                raise ValueError

    async def _run_plasma_processor(self):
        proc = plasma_processor.SimpleProcessor((self.config['reception'].get('plasma_path', '/tmp/plasma')))
        while True:
            try:
                await asyncio.sleep(1.0)
                rtn = proc.process(1.0)
            except ValueError as e:
                print(f"While processing failed to unpickle the test vector")
                raise e
            except KeyboardInterrupt:
                print(f"Done")
                break
        try:
            self._test_model(proc.model)
        except Exception as e:
            print(f"While processing failed to unpickle the model vector")
            raise e
        try:
            self._test_config(proc.config)
        except Exception as e:
            print(f"While processing failed to unpickle the config vector")
            raise e

    def  _test_model(self,tm):
        # I know that for this test measurement set that the time dump below has the 3rd baseline equal to
        # 4941813965.129038 -558.0393638815731 -692.2877732999623 -0.7558897212147713

        time_idx, data = tm.get_matching_data(4941813965.129038)

        assert time_idx == 2, "Time index was supposed to be 0"
        assert data.time == 4941813965.129038, "Time was supposed to match but does not"
        assert data.uu[2] == -558.0393638815731, "U distance in m was supposed to match but does not"
        assert data.vv[2] == -692.2877732999623, "V distance in m was supposed to match but does not"
        assert data.ww[2] == -0.7558897212147713, "W distance in m was supposed to match but does not"

    def _test_config(self,config):

        test_val = int(config['reception'].get('test_entry', 0))
        if test_val != 5:
            raise ValueError


    async def _task1(self):
        await asyncio.sleep(2.0)

    async def _task2(self):
        await asyncio.sleep(2.0)

    async def _test_create(self):
        self._test_plasma_writer_config()
        self.assertTrue(await self._create_plasma_writer())

    async def _test_write(self):
        self._test_plasma_writer_config()
        await asyncio.gather(self._run_plasma_processor(),self._run_plasma_writer())
    async def _test_gather(self):
        await asyncio.gather(self._task1(),self._task2())

    def test_create(self):
        loop = asyncio.new_event_loop()
        loop.run_until_complete(self._test_create())
        loop.close()

    def test_write(self):
         loop = asyncio.new_event_loop()
         loop.run_until_complete(self._test_write())
         loop.close()

    def test_gather(self):
        loop = asyncio.new_event_loop()
        loop.run_until_complete(self._test_gather())
        loop.close()

