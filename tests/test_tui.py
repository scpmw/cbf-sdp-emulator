import subprocess
import sys
import tempfile
import threading
import time
import unittest


class TestTUI(unittest.TestCase):

    def test_send_recv(self):
        """Start a send/recv process pair, check it works"""

        OUTPUT_MS = tempfile.mktemp(".ms", "output")
        INPUT_MS = 'tests/data/sim-vis.ms'
        recv_cmdline = [
            sys.executable, '-m', 'cbf_sdp.receiver', '-v',
            '-o', 'transmission.channels_per_stream=4',
            '-o', f'reception.outputfilename={OUTPUT_MS}',
            '-o', f'reception.datamodel={INPUT_MS}',
        ]
        send_cmdline = [
            sys.executable, '-m', 'cbf_sdp.packetiser', INPUT_MS,
            '-o', 'transmission.channels_per_stream=4',
            '-o', 'transmission.rate=147000'
        ]

        # Start receiver and wait until it's opened its UDP socket
        recv_proc = subprocess.Popen(recv_cmdline, stdout=subprocess.PIPE, shell=False)
        reading_attempts = 100
        timeout = 5
        while reading_attempts and timeout and not recv_proc.poll() is None:
            if not recv_proc.stdout.readable():
                time.sleep(0.2)
                timeout -= 0.2
                continue
            elif b'Started udp_reader' in recv_proc.stdout.readline():
                break
            reading_attempts -= 1
        self.assertTrue(recv_proc.poll() is None, "receiver finished prematurely")
        self.assertTrue(reading_attempts, "Couldn't determine whether receiver started correctly")

        # Run "in the background"
        def _run_recv():
            recv_proc.communicate()
        recv_thread = threading.Thread(target=_run_recv)
        recv_thread.daemon = True
        recv_thread.start()

        # Run sender until completion, then wait receiver to finish too
        send_proc = subprocess.Popen(send_cmdline, shell=False)
        send_proc.communicate()
        self.assertEqual(0, send_proc.returncode)
        recv_thread.join()

        self.assertEqual(0, recv_proc.returncode)