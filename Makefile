# Use bash shell with pipefail option enabled so that the return status of a
# piped command is the value of the last (rightmost) commnand to exit with a
# non-zero status. This lets us pipe output into tee but still exit on test
# failures.
SHELL = /bin/bash
.SHELLFLAGS = -o pipefail -c

all: test lint

# The following steps copy across useful output to this volume which can
# then be extracted to form the CI summary for the test procedure.
test:
	pip3 install -r test-requirements.txt
	mkdir -p build || true
	python3 setup.py test | tee ./build/setup_py_test.stdout
	mv coverage.xml ./build/reports/code-coverage.xml

# The following steps copy across useful output to this volume which can
# then be extracted to form the CI summary for the test procedure.
lint:
	pip3 install -r test-requirements.txt
	mkdir -p build/reports || true
	pylint --output-format=parseable cbf_sdp | tee ./build/code_analysis.stdout
	pylint --output-format=pylint2junit.JunitReporter cbf_sdp > ./build/reports/linting.xml


.PHONY: all test lint
